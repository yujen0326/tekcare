﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeKCareWeb.Vo
{
    public class RepairsSvcRequest
    {
        public string m_user { get; set; }
        public string accessToken { get; set; }
        public string responseCode { get; set; }
        public string responseMessage { get; set; }


    }
}

