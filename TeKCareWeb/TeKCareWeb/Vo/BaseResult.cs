﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeKCareWeb.Controllers.Vo
{
    public class BaseResult
    {
        private ProcessStatus _processStatus = new ProcessStatus();

        public ProcessStatus processStatus
        {
            get { return _processStatus; }
            set { _processStatus = value; }
        }
    }
}