﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeKCareWeb.Vo
{
    public class MOnsitenoteStagingStatus
    {
        public string tek_Id { get; set; }
        public string tek_no { get; set; }
        public string isLoad { get; set; }
        public string isCancel { get; set; }
        public string isStage { get; set; }
    }
}