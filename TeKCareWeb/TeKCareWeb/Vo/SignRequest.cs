﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeKCareWeb.Vo
{
    public class SignRequest
    {
        public string empId { get; set; }
        public string empPwd { get; set; }
        public string empToken { get; set; }
    }
}