﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeKCareWeb.Vo
{
    public class OnsiteNotesSvcAppRequest
    {
        public string m_user { get; set; }
        public string accessToken { get; set; }
        public List<MOnsitenoteStagingStatus> mOnsitenoteStagingStatus { get; set; }
    }
}

