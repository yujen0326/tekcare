﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeKCareWeb.Vo
{
    public class OnsiteNotesSvc
    {
        public string ID { get; set; }
        public string 客戶名稱 { get; set; }
        public string 收件單 { get; set; }
        public string 維修單號 { get; set; }
        public string 留言類別 { get; set; }
        public string 內容 { get; set; }
        public string 建立者 { get; set; }
        public string 建立時間 { get; set; }
        public string 工程師 { get; set; }
        public string 工程師Token { get; set; }
        public string 更新狀態 { get; set; }
    }
}