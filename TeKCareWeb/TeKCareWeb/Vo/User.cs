﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeKCareWeb.Vo
{
    public class User
    {
        public string sn { get; set; }
        public string empId { get; set; }
        public string m_user { get; set; }
		public string status { get; set; }
    }
}