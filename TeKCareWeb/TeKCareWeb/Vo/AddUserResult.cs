﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeKCareWeb.Controllers.Vo;
using TeKCareWeb.Vo;

namespace TeKCareWeb.Vo
{
    public class AddUserResult : BaseResult
    {
        public AddUser result { get; set; }
    }
}