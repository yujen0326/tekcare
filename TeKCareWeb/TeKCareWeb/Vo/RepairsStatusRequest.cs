﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeKCareWeb.Vo
{
    public class RepairsStatusRequest
    {
        public string m_user { get; set; }
        public string repair_no { get; set; }
        public string accessToken { get; set; }
        public string status { get; set; }
        public string updateDatetime { get; set; }
        public string progress_update_date { get; set; }

    }
}

