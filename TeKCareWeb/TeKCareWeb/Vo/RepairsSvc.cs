﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeKCareWeb.Vo
{
    public class RepairsSvc
    {
        public string ID { get; set; }
        public string 維修狀態 { get; set; }
        public string 客戶名稱 { get; set; }
        public string 維修單號 { get; set; }
        public string 維修開始時間 { get; set; }
        public string 服務方式 { get; set; }
        public string 服務項目 { get; set; }
        public string 產品名稱 { get; set; }
        public string 序號 { get; set; }
        public string 保固狀態 { get; set; }
        public string 聯絡人 { get; set; }
        public string 聯絡電話 { get; set; }
        public string 手機 { get; set; }
        public string 客戶地址 { get; set; }
        public string 收件備註 { get; set; }
        public string 工程師 { get; set; }
        public string 客戶留言 { get; set; }
        public string 故障描述 { get; set; }
        public string 工程師ID { get; set; }
        public string 產品大類 { get; set; }
        public string 產品小類 { get; set; }
        public string 保固開始日 { get; set; }
        public string 保固結束日 { get; set; }
        public string 經銷商名稱 { get; set; }
        public string 經銷商聯絡人 { get; set; }
        public string 經銷商電話 { get; set; }
        public string 更新狀態 { get; set; }

    }
}