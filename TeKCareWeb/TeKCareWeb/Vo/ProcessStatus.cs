﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeKCareWeb.Controllers.Vo
{
    public class ProcessStatus
    {
        private string _code = "100";
        public string resultCode
        {
            get { return _code; }
            set { _code = value; }
        }

        private string _message = "success";
        public String resultMessage
        {
            get { return _message; }
            set { _message = value; }
        }

        private string _token = "eyJhbGciOiJIUzI1NiJ9.eyJzdG9yZUlkIjpudWxsLCJleHAiOjE1NTM0MzQ4NTcsInVzZXJJZCI6MX0.oAZLHQQMQh18_RZlXVbGh0htG7NRQO10t1hK_JKjyzU";
        public String token
        {
            get { return _token; }
            set { _token = value; }
        }
    }
}