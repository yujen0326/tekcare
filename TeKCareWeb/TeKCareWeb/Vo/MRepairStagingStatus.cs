﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeKCareWeb.Vo
{
    public class MRepairStagingStatus
    {
        public string tek_no { get; set; }
        public string isLoad { get; set; }
        public string isCancel { get; set; }
        public string isStage { get; set; }
    }
}