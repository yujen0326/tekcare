﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeKCareWeb.Util;
using TeKCareWeb.Vo;

namespace TeKCareWeb.Service
{

    public class UserService
    {
        private List<Models.M_Mobile_UserInfo> mMobileUserInfoList;

        public User getQueryUser(string m_user)
        {
            User user = new User();

            using (var ctx = new Models.CRM_StagingEntities())
            {
                var q = ctx.M_Mobile_UserInfo.Where(x1 => x1.tek_user_id == m_user);
                if (q != null)
                {
                    mMobileUserInfoList = q.ToList();
                    int count = mMobileUserInfoList.Count();
                    if (count > 0)
                    {
                        user.sn = mMobileUserInfoList.FirstOrDefault().sn.ToString();
                        user.empId = mMobileUserInfoList.FirstOrDefault().tek_user_ad;
                        user.m_user = mMobileUserInfoList.FirstOrDefault().tek_user_id;
                        user.status = mMobileUserInfoList.FirstOrDefault().status;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return user;
        }

        public AddUser insUser(string empId, string m_user)
        {
            AddUser addUser = new AddUser();

            using (var ctx = new Models.CRM_StagingEntities())
            {
                var q = ctx.M_Mobile_UserInfo.Where(x1 => x1.tek_user_id == m_user);
                if (q != null)
                {
                    mMobileUserInfoList = q.ToList();
                    int count = mMobileUserInfoList.Count();
                    if (count > 0)
                    {
                        addUser.addStatus = "500";
                        return addUser;
                    }
                }

                var q1 = ctx.M_Mobile_UserInfo.Where(x1 => x1.tek_user_ad == empId);
                if (q1 != null)
                {
                    mMobileUserInfoList = q1.ToList();
                    int count1 = mMobileUserInfoList.Count();
                    if (count1 > 0)
                    {
                        addUser.addStatus = "500";
                        return addUser;
                    }
                }
                Models.M_Mobile_UserInfo ms1 = new Models.M_Mobile_UserInfo();
                ms1.tek_user_ad = empId;
                ms1.tek_user_id = m_user;
                ms1.tek_user_pwd = m_user;
                ms1.status = "1";
                ctx.M_Mobile_UserInfo.Add(ms1);
                int i = ctx.SaveChanges();
                if (i > 0)
                {
                    addUser.addStatus = "100";
                    return addUser;
                }
                else
                {
                    addUser.addStatus = "200";
                    return addUser;
                }
            }
        }

        public UpdUser updUser(string sn, string empId, string m_user, string status)
        {
            UpdUser updUser = new UpdUser();

            using (var ctx = new Models.CRM_StagingEntities())
            {
                int pk = Convert.ToInt32(sn);

                //判斷
                var q = ctx.M_Mobile_UserInfo.Where(x1 => x1.tek_user_id == m_user && x1.sn!= pk);
                if (q != null)
                {
                    mMobileUserInfoList = q.ToList();
                    int count = mMobileUserInfoList.Count();
                    if (count > 0)
                    {
                        updUser.updStatus = "500";
                        return updUser;
                    }
                }

                var q1 = ctx.M_Mobile_UserInfo.Where(x1 => x1.tek_user_ad == empId && x1.sn != pk);
                if (q1 != null)
                {
                    mMobileUserInfoList = q1.ToList();
                    int count1 = mMobileUserInfoList.Count();
                    if (count1 > 0)
                    {
                        updUser.updStatus = "500";
                        return updUser;
                    }
                }
                try
                {
                    var ms1 = ctx.M_Mobile_UserInfo.Find(pk);
                    ms1.tek_user_ad = empId;
                    ms1.tek_user_id = m_user;
                    ms1.tek_user_pwd = m_user;
                    ms1.status = status;
                    var i = ctx.SaveChanges();
                    updUser.updStatus = "100";
                    return updUser;
                }
                catch (Exception ex)
                {
                    updUser.updStatus = "200";
                    return updUser;
                }
            }
        }
    }
}