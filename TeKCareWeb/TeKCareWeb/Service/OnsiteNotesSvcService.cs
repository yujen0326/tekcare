﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeKCareWeb.Vo;
using TeKCareWeb.Service;
using TeKCareWeb.Util;
using System.Web.Services.Description;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TeKCareWeb.Service
{
    public class OnsiteNotesSvcService
    {
        private List<Models.Onsitenote_Staging> QueryOnsitenoteStagingResultList;
        private List<Models.M_Onsitenote_Staging> mOnsitenoteStagingList;

        public List<OnsiteNotesSvc> newOnSiteNoteList(string m_user)
        {
            return getNewOnSiteNoteList(m_user);
        }


        /// <summary>
        /// 取新OnSite留言資料
        /// </summary>
        /// <param name="m_user"></param>
        /// <returns></returns>
        private List<OnsiteNotesSvc> getNewOnSiteNoteList(String m_user)
        {
            List<OnsiteNotesSvc> onsiteNotesSvcList = new List<OnsiteNotesSvc>();
            using (var ctx = new Models.CRM_StagingEntities())
            {
                Models.Onsitenote_Staging OnsitenoteStagingList = new Models.Onsitenote_Staging();

                var q = ctx.Onsitenote_Staging.Where(x1 => x1.tek_m_user == m_user && !x1.tek_repair_no.Equals(""));
                if (q != null)
                {
                    QueryOnsitenoteStagingResultList = q.ToList();
                    int count = QueryOnsitenoteStagingResultList.Count();

                    if (count > 0)
                    {
                        for (int i = 0; i < count; i++)
                        {
                            DateTime? CreatedonTime = null;
                            CreatedonTime = QueryOnsitenoteStagingResultList[i].createdon;

                            OnsiteNotesSvc onsiteNotesSvc = new OnsiteNotesSvc();
                            onsiteNotesSvc.ID = QueryOnsitenoteStagingResultList[i].Id.ToString();
                            onsiteNotesSvc.客戶名稱 = QueryOnsitenoteStagingResultList[i].tek_serviceaccount;
                            onsiteNotesSvc.收件單 = QueryOnsitenoteStagingResultList[i].tek_workorder_no;
                            onsiteNotesSvc.維修單號 = QueryOnsitenoteStagingResultList[i].tek_repair_no;
                            onsiteNotesSvc.留言類別 = QueryOnsitenoteStagingResultList[i].tek_notetype;
                            onsiteNotesSvc.內容 = QueryOnsitenoteStagingResultList[i].tek_note;
                            onsiteNotesSvc.建立者 = QueryOnsitenoteStagingResultList[i].createdby;
                            onsiteNotesSvc.建立時間 = CreatedonTime?.ToString("yyyy/MM/dd HH:mm:ss");
                            onsiteNotesSvc.工程師 = QueryOnsitenoteStagingResultList[i].tek_m_user;
                            onsiteNotesSvc.工程師Token = QueryOnsitenoteStagingResultList[i].tek_m_user_token;
                            onsiteNotesSvc.更新狀態 = QueryOnsitenoteStagingResultList[i].Status;

                            string mOnsiteNotesSvgStatus = QueryOrInsertmOnsiteNotesSvc(onsiteNotesSvc);
                            if (!mOnsiteNotesSvgStatus.Equals("repeat"))
                            {
                                onsiteNotesSvcList.Add(onsiteNotesSvc);
                            }
                        }
                        return onsiteNotesSvcList;
                    }
                    else { return null; }
                }
                else
                {
                    return null;
                }
            }
        }

        public List<OnsiteNotesSvc> newNotCloseOnsiteNote(string m_user, string status)
        {
            return getNotCloseOnsiteNoteList(m_user, status);
        }

        /// <summary>
        /// 取新派工案件清單資料
        /// </summary>
        /// <param name="m_user"></param>
        /// <returns></returns>
        private List<OnsiteNotesSvc> getNotCloseOnsiteNoteList(String m_user, String status)
        {
            List<OnsiteNotesSvc> onsiteNotesSvcList = new List<OnsiteNotesSvc>();

            TeKCareWeb.mobileService.MobileService api0 = new TeKCareWeb.mobileService.MobileService();
            TeKCareWeb.tekcare.Tek_WebMobileSvc api = new TeKCareWeb.tekcare.Tek_WebMobileSvc();
            Object o = api.GetOnsiteNotesSvc(m_user, status);
            Util.Utils.errorLog(o.ToString(), "getNotCloseOnsiteNote");

            JArray obj = (JArray)JsonConvert.DeserializeObject(o.ToString());
            foreach (JObject content in obj.Children<JObject>())
            {
                OnsiteNotesSvc onsiteNotesSvc = new OnsiteNotesSvc();

                foreach (JProperty prop in content.Properties())
                {
                    string tempName = prop.Name.ToString();
                    string tempValue = prop.Value.ToString();
                    if (tempName.Equals("客戶名稱 ")) { onsiteNotesSvc.客戶名稱 = tempValue; }
                    if (tempName.Equals("收件單")) { onsiteNotesSvc.收件單 = tempValue; }
                    if (tempName.Equals("維修單號")) { onsiteNotesSvc.維修單號 = tempValue; }
                    if (tempName.Equals("留言類別")) { onsiteNotesSvc.留言類別 = tempValue; }
                    if (tempName.Equals("內容")) { onsiteNotesSvc.內容 = tempValue; }
                    if (tempName.Equals("建立者")) { onsiteNotesSvc.建立者 = tempValue; }
                    if (tempName.Equals("建立時間")) { onsiteNotesSvc.建立時間 = tempValue; }
                    if (tempName.Equals("工程師")) { onsiteNotesSvc.工程師 = tempValue; }
                    if (tempName.Equals("更新狀態")) { onsiteNotesSvc.更新狀態 = tempValue; }
                }
                onsiteNotesSvcList.Add(onsiteNotesSvc);
            }
            return onsiteNotesSvcList;
        }

        /// <summary>
        /// 暫存資料表M_Onsitenote_Staging讀取或暫存
        /// </summary>
        /// <param name="OnsiteNotesSvc"></param>
        /// <returns></returns>
        public string QueryOrInsertmOnsiteNotesSvc(OnsiteNotesSvc onsiteNotesSvc)
        {
            using (var ctx = new Models.CRM_StagingEntities())
            {
                int tek_Id = Convert.ToInt32(onsiteNotesSvc.ID);
                string isLoad = "";
                var q = ctx.M_Onsitenote_Staging.Where(x1 => x1.tek_Id == tek_Id);

                if (q != null)
                {
                    mOnsitenoteStagingList = q.ToList();
                    int count = mOnsitenoteStagingList.Count();
                    if (count > 0)
                    {
                        isLoad = (mOnsitenoteStagingList.FirstOrDefault().isLoad == null) ? "" : mOnsitenoteStagingList.FirstOrDefault().isLoad;

                        if (isLoad.Equals("1"))
                        {
                            return "repeat";
                        }
                        else
                        {
                            return "appNotLoad";
                        }
                    }
                    else
                    {
                        Models.M_Onsitenote_Staging m_Onsitenote_Staging = new Models.M_Onsitenote_Staging();
                        m_Onsitenote_Staging.tek_Id = tek_Id;
                        m_Onsitenote_Staging.tek_serviceaccount = onsiteNotesSvc.客戶名稱;
                        m_Onsitenote_Staging.tek_workorder_no = onsiteNotesSvc.收件單;
                        m_Onsitenote_Staging.tek_repair_no = onsiteNotesSvc.維修單號;
                        m_Onsitenote_Staging.tek_notetype = onsiteNotesSvc.留言類別;
                        m_Onsitenote_Staging.tek_note = onsiteNotesSvc.內容;
                        m_Onsitenote_Staging.tek_m_user = onsiteNotesSvc.工程師;
                        m_Onsitenote_Staging.tek_m_user_token = onsiteNotesSvc.工程師Token;
                        m_Onsitenote_Staging.createdby = onsiteNotesSvc.建立者;
                        if (!onsiteNotesSvc.建立時間.Equals(""))
                        {
                            m_Onsitenote_Staging.createdon = Convert.ToDateTime(onsiteNotesSvc.建立時間);
                        }
                        m_Onsitenote_Staging.Status = onsiteNotesSvc.更新狀態;
                        m_Onsitenote_Staging.isNew = "0";
                        m_Onsitenote_Staging.newDatetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                        m_Onsitenote_Staging.isLoad = "0";
                        m_Onsitenote_Staging.loadDatetime = null;
                        m_Onsitenote_Staging.isCancel = "0";
                        m_Onsitenote_Staging.cancelDatetime = null;
                        m_Onsitenote_Staging.isStage = "0";
                        m_Onsitenote_Staging.caseType = "0";
                        m_Onsitenote_Staging.caseStatus = "1";
                        ctx.M_Onsitenote_Staging.Add(m_Onsitenote_Staging);
                        int i = ctx.SaveChanges();

                        if (i > 0)
                        {
                            return "true";
                        }
                        else
                        {
                            return "false";
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public OnsiteNotesSvcApp sendOnsiteNotesSvcByApp(string m_user, string accessToken, List<MOnsitenoteStagingStatus> tekNoList)
        {
            OnsiteNotesSvcApp onsiteNotesSvcApp = new OnsiteNotesSvcApp();
            int tek_Id = 0;
            string tek_workorder_no = "";

            int id = 0;
            int count = tekNoList.Count();
            if (count > 0)
            {
                using (var ctx = new Models.CRM_StagingEntities())
                {
                    for (int i = 0; i < count; i++)
                    {
                        if (tekNoList[i].tek_Id != null) { tek_Id = Convert.ToInt32(tekNoList[i].tek_Id); }
                        if (tekNoList[i].tek_no != null) { tek_workorder_no = tekNoList[i].tek_no; }
                        var q = ctx.M_Onsitenote_Staging.Where(x1 => x1.tek_Id == tek_Id && x1.tek_workorder_no == tek_workorder_no && x1.tek_m_user == m_user);

                        if (q != null)
                        {
                            mOnsitenoteStagingList = q.ToList();
                            int count1 = mOnsitenoteStagingList.Count();
                            if (count1 > 0)
                            {
                                id = mOnsitenoteStagingList.FirstOrDefault().Id;
                                var u = ctx.M_Onsitenote_Staging.Find(id);
                                if (tekNoList[i].isLoad != null)
                                {
                                    u.isLoad = tekNoList[i].isLoad;
                                    u.loadDatetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                                }
                                if (tekNoList[i].isCancel != null)
                                {
                                    if (tekNoList[i].isCancel.Equals("1"))
                                    {
                                        u.isCancel = tekNoList[i].isCancel;
                                        u.cancelDatetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                                    }
                                }
                                if (tekNoList[i].isStage != null)
                                {
                                    u.isStage = tekNoList[i].isStage;
                                }
                                ctx.SaveChanges();
                            }
                        }
                    }
                }
            }
            return onsiteNotesSvcApp;
        }
    }
}