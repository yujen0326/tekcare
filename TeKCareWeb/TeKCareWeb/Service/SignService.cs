﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeKCareWeb.Util;
using TeKCareWeb.Vo;

namespace TeKCareWeb.Service
{
    public class SignService
    {
        private List<Models.M_Mobile_UserInfo> mMobileUserInfoList;
        private List<Models.M_Mobile_UserInfo_Token> mMobileUserInfoTokenList;

        public String generateToken(String m_user)
        {
            //工程師ID + 系統時間
            string dateTimeStr = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            var payload = new Dictionary<string, string>
            {
                { "m_user", m_user },
                { "time", dateTimeStr }
            };
            string jwt = Utils.EnCodeJwt(payload);
            return Utils.EnCodeJwt(payload);
        }

        public Sign Login(string empId, string empPwd, string empToken)
        {

            Sign sign = new Sign();
            int i = 0;
            bool boolLdap = Utils.ValidateLDAPUser(empId, empPwd);
            //bool boolLdap = true;
            if (boolLdap == true)
            {
                //AD認證OK
                using (var ctx = new Models.CRM_StagingEntities())
                {
                    var q = ctx.M_Mobile_UserInfo.Where(x1 => x1.tek_user_ad == empId && x1.status == "1");
                    if (q != null)
                    {
                        mMobileUserInfoList = q.ToList();
                        int count = mMobileUserInfoList.Count();
                        if (count > 0)
                        {
                            string tek_user_id = mMobileUserInfoList.FirstOrDefault().tek_user_id;
                            //檢查LOGIN通過後
                            //TOKEN insert or update table
                            var q1 = ctx.M_Mobile_UserInfo_Token.Where(x1 => x1.tek_user_id == tek_user_id && x1.status == "1");
                            mMobileUserInfoTokenList = q1.ToList();
                            int count1 = mMobileUserInfoTokenList.Count();
                            if (count1 > 0)
                            {
                                int pksn = mMobileUserInfoTokenList.FirstOrDefault().sn;
                                var u = ctx.M_Mobile_UserInfo_Token.Find(pksn);
                                u.tek_user_token = empToken;
                                u.newDatetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                                i = ctx.SaveChanges();
                            }
                            else
                            {
                                Models.M_Mobile_UserInfo_Token m_Mobile_UserInfo_Token = new Models.M_Mobile_UserInfo_Token();
                                m_Mobile_UserInfo_Token.tek_user_id = tek_user_id;
                                m_Mobile_UserInfo_Token.tek_user_token = empToken;
                                m_Mobile_UserInfo_Token.newDatetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                                m_Mobile_UserInfo_Token.status = "1";
                                ctx.M_Mobile_UserInfo_Token.Add(m_Mobile_UserInfo_Token);
                                i = ctx.SaveChanges();
                            }

                            if (i > 0)
                            {
                                sign.empId = empId;
                                sign.m_user = mMobileUserInfoList.FirstOrDefault().tek_user_id;
                                sign.accessToken = generateToken(mMobileUserInfoList.FirstOrDefault().tek_user_id);
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            else
            {
                return null;
            }
            return sign;
        }
    }
}