﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeKCareWeb.Vo;
using TeKCareWeb.Service;
using TeKCareWeb.Util;
using System.Web.Services.Description;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TeKCareWeb.Service
{
    public class RepairsSvcService
    {

        private List<Models.M_Repair_Staging> mRepairStagingList;
        private List<Models.Repair_Staging> RepairStagingList;
        private List<Models.M_Mobiletime_Staging> mMobiletimeStagingList;

        public List<RepairsSvc> newRepairsSvc(string m_user, string status)
        {
            return getRepairsSvcList(m_user, status);
        }

        /// <summary>
        /// 取新派工案件清單資料
        /// </summary>
        /// <param name="m_user"></param>
        /// <returns></returns>
        private List<RepairsSvc> getRepairsSvcList(String m_user, String status)
        {
            string tek_name = "";
            int idnum = 1;
            List<RepairsSvc> repairsSvcList = new List<RepairsSvc>();

            TeKCareWeb.tekcare.Tek_WebMobileSvc api = new TeKCareWeb.tekcare.Tek_WebMobileSvc();
            Object o = api.GetRepairsSvc(m_user, status);

            if (status.Equals("1"))
            {
                Util.Utils.errorLog(o.ToString(), "getRepairsSvc");
            }
            else
            {
                Util.Utils.errorLog(o.ToString(), "getNotCloseRepair");
            }


            JArray objFirst = (JArray)JsonConvert.DeserializeObject(o.ToString());

            JArray obj = new JArray(objFirst.OrderBy(x => (string)x["維修單號"]));
            RepairsSvc oldRepairsSvc = new RepairsSvc();
            foreach (JObject content in obj.Children<JObject>())
            {
                RepairsSvc repairsSvc = new RepairsSvc();

                foreach (JProperty prop in content.Properties())
                {
                    string tempName = prop.Name.ToString();
                    string tempValue = prop.Value.ToString();

                    if (tempName.Equals("ID")) {
                        repairsSvc.ID = tempValue;
                        if (repairsSvc.ID.Equals(""))
                        {
                            if (status.Equals("2"))
                            {
                                repairsSvc.ID = idnum.ToString();
                            }
                        }
                    }
                    if (tempName.Equals("維修狀態")) { repairsSvc.維修狀態 = tempValue; }
                    if (tempName.Equals("客戶名稱")) { repairsSvc.客戶名稱 = tempValue; }
                    if (tempName.Equals("維修單號")) { repairsSvc.維修單號 = tempValue; }
                    if (tempName.Equals("維修開始時間"))
                    {
                        //repairsSvc.維修開始時間 = tempValue;
                        if (!tempValue.Equals(""))
                        {
                            DateTime t = Convert.ToDateTime(tempValue);
                            string s = t.ToString("yyyy/MM/dd HH:mm:ss");
                            repairsSvc.維修開始時間 = s.ToString();
                        }
                        else
                        {
                            repairsSvc.維修開始時間 = tempValue;
                        }
                    }
                    if (tempName.Equals("服務方式")) { repairsSvc.服務方式 = tempValue; }
                    if (tempName.Equals("服務項目")) { repairsSvc.服務項目 = tempValue; }
                    if (tempName.Equals("產品名稱")) { repairsSvc.產品名稱 = tempValue; }
                    if (tempName.Equals("序號")) { repairsSvc.序號 = tempValue; }
                    if (tempName.Equals("保固狀態")) { repairsSvc.保固狀態 = tempValue; }
                    if (tempName.Equals("聯絡人")) { repairsSvc.聯絡人 = tempValue; }
                    if (tempName.Equals("聯絡電話")) { repairsSvc.聯絡電話 = tempValue; }
                    if (tempName.Equals("手機")) { repairsSvc.手機 = tempValue; }
                    if (tempName.Equals("客戶地址")) { repairsSvc.客戶地址 = tempValue; }
                    if (tempName.Equals("收件備註")) { repairsSvc.收件備註 = tempValue; }
                    if (tempName.Equals("工程師")) { repairsSvc.工程師 = tempValue; }
                    if (tempName.Equals("客戶留言")) { repairsSvc.客戶留言 = tempValue; }
                    if (tempName.Equals("故障描述")) { repairsSvc.故障描述 = tempValue; }
                    if (tempName.Equals("工程師ID")) { repairsSvc.工程師ID = tempValue; }
                    if (tempName.Equals("產品大類")) { repairsSvc.產品大類 = tempValue; }
                    if (tempName.Equals("產品小類")) { repairsSvc.產品小類 = tempValue; }
                    if (tempName.Equals("保固開始日")) {
                        if (!tempValue.Equals(""))
                        {
                            DateTime t1 = Convert.ToDateTime(tempValue);
                            string s1 = t1.ToString("yyyy/MM/dd");
                            repairsSvc.保固開始日 = t1.ToString();
                        }
                        else
                        {
                            repairsSvc.保固開始日 = tempValue;
                        }
                    }
                    if (tempName.Equals("保固結束日")) {
                        //repairsSvc.保固結束日 = tempValue;
                        if (!tempValue.Equals(""))
                        {
                            DateTime t2 = Convert.ToDateTime(tempValue);
                            string s2 = t2.ToString("yyyy/MM/dd");
                            repairsSvc.保固結束日 = t2.ToString();
                        }
                        else
                        {
                            repairsSvc.保固結束日 = tempValue;
                        }
                    }
                    if (tempName.Equals("經銷商名稱")) { repairsSvc.經銷商名稱 = tempValue; }
                    if (tempName.Equals("經銷商連絡人")) { repairsSvc.經銷商聯絡人 = tempValue; }
                    if (tempName.Equals("經銷商電話")) { repairsSvc.經銷商電話 = tempValue; }
                    if (tempName.Equals("更新狀態")) { repairsSvc.更新狀態 = tempValue; }
                    idnum++;
                }

                //if (repairsSvc.維修單號.Equals("2111191104176"))
                //{
                //    string a = repairsSvc.維修單號;
                //    string b = "check";
                //}

                if (!repairsSvc.更新狀態.Equals("")) {
                    if (!status.Equals("2"))
                    {
                        string mRepairStagingStatus = QueryOrInsertmRepairStaging(repairsSvc, m_user);
                        if (mRepairStagingStatus.Equals("update"))
                        {
                            DateTime t1 = Convert.ToDateTime(repairsSvc.維修開始時間);
                            if(t1> Convert.ToDateTime("2019/08/07"))
                            {
                                repairsSvcList.Add(repairsSvc);
                            }
                        }
                        else if (mRepairStagingStatus.Equals("cancel"))
                        {
                            //oldRepairsSvc = new RepairsSvc();
                            repairsSvcList.Add(repairsSvc);
                            tek_name = repairsSvc.維修單號;
                            //oldRepairsSvc = repairsSvc;
                        }
                        else if (mRepairStagingStatus.Equals("cancelToAgain"))
                        {
                            //repairsSvcList.Remove(oldRepairsSvc);
                            repairsSvcList.Add(repairsSvc);
                            tek_name = repairsSvc.維修單號;
                        }
                        else if (!mRepairStagingStatus.Equals("repeat"))
                        {
                            if (repairsSvc.維修單號 != tek_name)
                            {
                                repairsSvcList.Add(repairsSvc);
                                tek_name = repairsSvc.維修單號;
                            }
                        }
                    }
                    else
                    {
                        if (repairsSvc.維修單號 != tek_name)
                        {
                            repairsSvcList.Add(repairsSvc);
                            tek_name = repairsSvc.維修單號;
                        }
                    }
                }
            }
            return repairsSvcList;
        }


        /// <summary>
        /// 暫存資料表M_Repair_Staging讀取或暫存
        /// </summary>
        /// <param name="RepairsSvc"></param>
        /// <returns></returns>
        public string QueryOrInsertmRepairStaging(RepairsSvc repairsSvc, String m_user)
        {
            using (var ctx = new Models.CRM_StagingEntities())
            {
                int tek_Id = Convert.ToInt32(repairsSvc.ID);
                string tek_name = repairsSvc.維修單號;
                string tek_changecode = repairsSvc.更新狀態;
                string isLoad = "";
                string isCancel = "";
                string tek_repairstatus = repairsSvc.維修狀態;
                if (tek_changecode.Equals("Update"))
                {
                    return "update";
                }

                var q = ctx.M_Repair_Staging.Where(x1 => x1.tek_name == tek_name && x1.tek_m_userid == m_user);

                if (q != null)
                {
                    mRepairStagingList = q.ToList();
                    int count = mRepairStagingList.Count();
                    if (count > 0)
                    {
                        if (repairsSvc.維修狀態.Equals("取消維修"))
                        {
                            int id = mRepairStagingList.FirstOrDefault().Id;
                            var u = ctx.M_Repair_Staging.Find(id);
                            if (!u.isCancel.Equals("1"))
                            {
                                u.tek_repairstatus = tek_repairstatus;
                                u.isCancel = "1";
                                u.cancelDatetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                                ctx.SaveChanges();
                                return "cancel";

                            }
                            else 
                            {
                                return "cancel";
                            }
                            //else if (u.isCancel.Equals("1") && u.isLoad.Equals("1"))
                            //{
                            //    return "repeat";
                            //}
                            //else
                            //{
                            //    return "appNotLoad";
                            //}
                        }
                        else
                        {
                            isLoad = (mRepairStagingList.FirstOrDefault().isLoad == null) ? "" : mRepairStagingList.FirstOrDefault().isLoad;
                            isCancel = (mRepairStagingList.FirstOrDefault().isCancel == null) ? "" : mRepairStagingList.FirstOrDefault().isCancel;


                            int id = mRepairStagingList.FirstOrDefault().Id;
                            var u = ctx.M_Repair_Staging.Find(id);
                            if (isLoad.Equals("1"))
                            {
                                if (isCancel.Equals("1"))
                                {
                                    u.tek_repairstatus = tek_repairstatus;
                                    u.isCancel = "0";
                                    u.cancelDatetime = null;
                                    ctx.SaveChanges();
                                    return "cancelToAgain";
                                }
                                else
                                {
                                    return "repeat";
                                }
                            }
                            else
                            {
                                if (isCancel.Equals("1"))
                                {
                                    u.tek_repairstatus = tek_repairstatus;
                                    u.isCancel = "0";
                                    u.cancelDatetime = null;
                                    ctx.SaveChanges();
                                    return "cancelToAgain";
                                }
                                else
                                {
                                    return "appNotLoad";
                                }
                            }
                        }
                    }
                    else
                    {
                        Models.M_Repair_Staging m_Repair_Staging = new Models.M_Repair_Staging();
                        m_Repair_Staging.tek_Id = tek_Id;
                        m_Repair_Staging.tek_repairstatus = repairsSvc.維修狀態;
                        m_Repair_Staging.tek_account = repairsSvc.客戶名稱;
                        m_Repair_Staging.tek_name = repairsSvc.維修單號;
                        if (!repairsSvc.維修開始時間.Equals(""))
                        {
                            m_Repair_Staging.tek_recipient_date = Convert.ToDateTime(repairsSvc.維修開始時間);
                        }
                        m_Repair_Staging.tek_service = repairsSvc.服務方式;
                        m_Repair_Staging.tek_service_item = repairsSvc.服務項目;
                        m_Repair_Staging.tek_serial_no = repairsSvc.序號;
                        m_Repair_Staging.tek_warrenty = repairsSvc.保固狀態;
                        m_Repair_Staging.tek_contact = repairsSvc.聯絡人;
                        m_Repair_Staging.tek_telephone = repairsSvc.聯絡電話;
                        m_Repair_Staging.tek_mobile = repairsSvc.手機;
                        m_Repair_Staging.tek_clientadd = repairsSvc.客戶地址;
                        m_Repair_Staging.tek_note = repairsSvc.收件備註;
                        m_Repair_Staging.Status = null;
                        m_Repair_Staging.tek_m_user = repairsSvc.工程師;
                        m_Repair_Staging.tek_remark = repairsSvc.客戶留言;
                        m_Repair_Staging.tek_product = repairsSvc.產品名稱;
                        m_Repair_Staging.tek_errordescip = repairsSvc.故障描述;
                        m_Repair_Staging.tek_m_userid = repairsSvc.工程師ID;
                        m_Repair_Staging.tek_bigtype = repairsSvc.產品大類;
                        m_Repair_Staging.tek_smalltype = repairsSvc.產品小類;

                        if (!repairsSvc.保固開始日.Equals(""))
                        {
                            m_Repair_Staging.tek_warrantystart = Convert.ToDateTime(repairsSvc.保固開始日);
                        }
                        if (!repairsSvc.保固結束日.Equals(""))
                        {
                            m_Repair_Staging.tek_warrantyend = Convert.ToDateTime(repairsSvc.保固結束日);
                        }
                        m_Repair_Staging.tek_distributor = repairsSvc.經銷商名稱;
                        m_Repair_Staging.tek_distributor_contact = repairsSvc.經銷商聯絡人;
                        m_Repair_Staging.tek_distributor_phone = repairsSvc.經銷商電話;
                        m_Repair_Staging.tek_changecode = repairsSvc.更新狀態;
                        m_Repair_Staging.isNew = "0";
                        m_Repair_Staging.newDatetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                        m_Repair_Staging.isLoad = "0";
                        m_Repair_Staging.loadDatetime = null;
                        if (repairsSvc.維修狀態.Equals("取消維修"))
                        {
                            m_Repair_Staging.isCancel = "1";
                            m_Repair_Staging.cancelDatetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                        }
                        else
                        {
                            m_Repair_Staging.isCancel = "0";
                            m_Repair_Staging.cancelDatetime = null;
                        }
                        m_Repair_Staging.isStage = "0";
                        m_Repair_Staging.caseType = "0";
                        m_Repair_Staging.caseStatus = "1";
                        try
                        {
                            ctx.M_Repair_Staging.Add(m_Repair_Staging);
                            int i = ctx.SaveChanges();
                            if (i > 0)
                            {
                                return "true";
                            }
                            else
                            {
                                return "false";
                            }
                        }
                        catch (Exception ex)
                        {
                            return "false";
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
        }



        /// <summary>
        /// 變更維修進度狀態
        /// </summary>
        /// <param name="m_user"></param>
        /// <param name="repair_no"></param>
        /// <param name="accessToken"></param>
        /// <param name="status"></param>
        /// <param name="updateDatetime"></param>
        /// <returns></returns>
        public RepairsStatus updateRepairStatus(string m_user, string repair_no, string accessToken, string status, string updateDatetime, string progress_update_date)
        {
            RepairsStatus repairsStatus = new RepairsStatus();

            string msg0 = getMobiletimeStaging(m_user, repair_no, status, progress_update_date);
            if (msg0.Equals("true"))
            {
                string msg = insertMobiletimeStaging(m_user, repair_no, status, updateDatetime, progress_update_date);
                if (!msg.Equals("false"))
                {
                    string newStr = msg + "_" + repair_no;
                    //string newStr = repair_no;

                    TeKCareWeb.mobileService.MobileService api = new TeKCareWeb.mobileService.MobileService();
                    Object o = api.SyncMobile("SyncMobiletime", newStr, "PLUGIN");
                    //記LOG
                    markMobiletimeStagingLog(msg);

                    return repairsStatus;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <param name="m_user"></param7>
        /// <param name="repair_no"></param>
        /// <param name="status"></param>
        /// <param name="updateDate"></param>
        /// <returns></returns>
        public string getMobiletimeStaging(string m_user, string repair_no, string status, string updateDate)
        {
            using (var ctx = new Models.CRM_StagingEntities())
            {
                DateTime updDate = Convert.ToDateTime(updateDate);

                var q = ctx.M_Mobiletime_Staging.Where(x1 => x1.tek_repair_tek_mobiletime == repair_no && x1.tek_m_user == m_user && x1.tek_m_status== status);
                if (q != null)
                {
                    mMobiletimeStagingList = q.ToList();
                    int count = mMobiletimeStagingList.Count();
                    if (count > 0)
                    {
                        DateTime progress_update = Convert.ToDateTime(mMobiletimeStagingList.FirstOrDefault().progress_update_date);
                        string progressStr = progress_update.ToString("yyyy/MM/dd");
                        string updDateStr = updDate.ToString("yyyy/MM/dd");

                        if (progressStr.Equals(updDateStr))
                        {
                            return "false";
                        }
                        else
                        {
                            return "true";
                        }
                    }
                    else
                    {
                        return "true";
                    }
                }
                else
                {
                    return "true";
                }
            }
        }



        /// <summary>
        /// 變更維修進度狀態-新增Mobiletime_Staging
        /// </summary>
        /// <param name="m_user"></param7>
        /// <param name="repair_no"></param>
        /// <param name="status"></param>
        /// <param name="updateDatetime"></param>
        /// <returns></returns>
        public string insertMobiletimeStaging(string m_user, string repair_no, string status, string updateDatetime, string progress_update_date)
        {
            string upadteRepairStaging = "0";

            using (var ctx = new Models.CRM_StagingEntities())
            {
                DateTime? tek_flag = Convert.ToDateTime(updateDatetime);
                DateTime? tek_feedback_date = Convert.ToDateTime(progress_update_date);

                Models.Mobiletime_Staging ms1 = new Models.Mobiletime_Staging();
                ms1.tek_repair_tek_mobiletime = repair_no;
                ms1.tek_m_user = m_user;
                ms1.tek_m_status = status;
                ms1.Status = "Waiting";
                if (status.Equals("3")) { ms1.tek_flag = tek_flag; }
                else if (status.Equals("6")) { ms1.tek_flag = tek_flag; }
                else if (status.Equals("5")) { upadteRepairStaging = "1"; }
                else if (status.Equals("7")) { upadteRepairStaging = "1"; }
                else if (status.Equals("9")) { upadteRepairStaging = "1"; }
                else { ms1.tek_flag = null; }
                ms1.Log = null;
                ms1.tek_GPS = null;
                ms1.source_id = "app";
                ms1.tek_feedback_date = tek_feedback_date;
                ctx.Mobiletime_Staging.Add(ms1);
                int i = ctx.SaveChanges();

                if (i > 0)
                {
                    Models.M_Mobiletime_Staging ms2 = new Models.M_Mobiletime_Staging();
                    ms2.tek_Id = ms1.Id;
                    ms2.tek_repair_tek_mobiletime = repair_no;
                    ms2.tek_m_user = m_user;
                    ms2.tek_m_status = status;
                    ms2.Status = "Waiting";
                    if (status.Equals("3")) { ms2.tek_flag = tek_flag; }
                    else if (status.Equals("6")) { ms2.tek_flag = tek_flag; }
                    else { ms2.tek_flag = null; }
                    ms2.Log = null;
                    ms2.tek_GPS = null;
                    ms2.newDatetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                    ms2.progress_update_date = progress_update_date;
                    ctx.M_Mobiletime_Staging.Add(ms2);
                    int j = ctx.SaveChanges();

                    if (j > 0){
                        if (upadteRepairStaging.Equals("1"))
                        {
                            var r = ctx.Repair_Staging.Where(x1 => x1.tek_name == repair_no && x1.tek_m_userid == m_user);
                            if (r != null)
                            {
                                RepairStagingList = r.ToList();
                                int count2 = RepairStagingList.Count();
                                if (count2 > 0)
                                {
                                    for (int z = 0; z < count2; z++)
                                    {
                                        int keyId = RepairStagingList[z].Id;
                                        Models.Repair_Staging repair_Staging = ctx.Repair_Staging.Find(keyId);
                                        repair_Staging.tek_repairstatus = "已結案";
                                        int x = ctx.SaveChanges();

                                        Util.Utils.errorLog(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), "updateRepairStatus");
                                        Util.Utils.errorLog("update Repair_Staging", "updateRepairStatus");
                                        Util.Utils.errorLog("Id=" + keyId.ToString(), "updateRepairStatus");
                                        Util.Utils.errorLog("repair_no=" + repair_no, "updateRepairStatus");
                                    }
                                }
                            }
                        }
                        return ms1.Id.ToString();
                    }
                    else
                    {
                        return "false";
                    }
                }
                else
                {
                    return "false";
                }
            }
        }

        /// <returns></returns>
        public void markMobiletimeStagingLog(string strId)
        {
            int Id = Convert.ToInt32(strId);

            using (var ctx = new Models.CRM_StagingEntities())
            {
                var r = ctx.Mobiletime_Staging.Find(Id);
                if (r != null)
                {
                    Util.Utils.errorLog(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), "updateRepairStatus");
                    Util.Utils.errorLog("呼叫翰資API", "updateRepairStatus");
                    Util.Utils.errorLog("tek_repair_tek_mobiletime =" + r.tek_repair_tek_mobiletime, "updateRepairStatus");
                    Util.Utils.errorLog("tek_m_status =" + r.tek_m_status, "updateRepairStatus");
                    Util.Utils.errorLog("tek_flag =" + r.tek_flag, "updateRepairStatus");
                    Util.Utils.errorLog("Status =" + r.Status, "updateRepairStatus");
                    Util.Utils.errorLog("tek_m_user =" + r.tek_m_user, "updateRepairStatus");
                }
            }
        }



        public RepairsSvcApp sendRepairsSvcByApp(string m_user, string accessToken, List<MRepairStagingStatus> tekNoList)
        {
            RepairsSvcApp repairsSvcAppList = new RepairsSvcApp();
            string tek_name = "";

            int id = 0;
            int count = tekNoList.Count();
            if (count > 0)
            {
                using (var ctx = new Models.CRM_StagingEntities())
                {
                    for (int i = 0; i < count; i++)
                    {
                        if (tekNoList[i].tek_no != null) { tek_name = tekNoList[i].tek_no; }
                        var q = ctx.M_Repair_Staging.Where(x1 => x1.tek_name == tek_name && x1.tek_m_userid == m_user);

                        if (q != null)
                        {
                            mRepairStagingList = q.ToList();
                            int count1 = mRepairStagingList.Count();
                            if (count1 > 0)
                            {
                                id = mRepairStagingList.FirstOrDefault().Id;
                                var u = ctx.M_Repair_Staging.Find(id);
                                if (tekNoList[i].isLoad != null)
                                {
                                    u.isLoad = tekNoList[i].isLoad;
                                    u.loadDatetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                                }
                                if (tekNoList[i].isCancel != null)
                                {
                                    if (tekNoList[i].isCancel.Equals("1"))
                                    {
                                        u.isCancel = tekNoList[i].isCancel;
                                        u.cancelDatetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                                    }
                                }
                                if (tekNoList[i].isStage != null) {
                                    u.isStage = tekNoList[i].isStage;
                                }
                                ctx.SaveChanges();
                            }
                        }
                    }
                }
            }
            return repairsSvcAppList;
        }


        public void setMobiletimeStaging()
        {
            //List<Models.Mobiletime_Staging> mobiletimeStagingList;

            //using (var ctx = new Models.CRM_StagingEntities())
            //{
            //    var q = ctx.Mobiletime_Staging.Where(x1 => x1.Status == "Waiting" && x1.source_id == "app" && x1.Id > 104622);
            //    if (q != null)
            //    {
            //        mobiletimeStagingList = q.ToList();
            //        int count = mobiletimeStagingList.Count();
            //        if (count > 0)
            //        {
            //            for (int i = 0; i < count; i++)
            //            {
            //                string tek_name = mobiletimeStagingList[i].tek_repair_tek_mobiletime;
            //                string id = mobiletimeStagingList[i].Id.ToString();
            //                string str = id + "_" + tek_name;
            //                TeKCareWeb.mobileService.MobileService api = new TeKCareWeb.mobileService.MobileService();
            //                Object o = api.SyncMobile("SyncMobiletime", str, "PLUGIN");
            //                //記LOG
            //                markMobiletimeStagingLog(id);
            //            }
            //        }
            //    }
            //}
        }

    }
}