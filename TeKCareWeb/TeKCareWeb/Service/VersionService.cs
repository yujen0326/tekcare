﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeKCareWeb.Util;
using TeKCareWeb.Vo;

namespace TeKCareWeb.Service
{
    public class VersionService
    {

        public GetVersion getVersionStr()
        {
            GetVersion getVersion = new GetVersion();
            using (var ctx = new Models.CRM_StagingEntities())
            {
                Models.App_Version app_Version = ctx.App_Version.Find(1);
                getVersion.version = app_Version.version;
            }
            return getVersion;
        }

        public SendVersion SendVersionStr(String newVersion)
        {
            SendVersion sendVersion = new SendVersion();
            using (var ctx = new Models.CRM_StagingEntities())
            {
                var u = ctx.App_Version.Find(1);
                u.version = newVersion;
                u.newDatetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                int i = ctx.SaveChanges();
                if (i > 0)
                {
                    sendVersion.version = newVersion;
                    sendVersion.newDatetime = u.newDatetime;
                }
                else
                {
                    return null;
                }
            }
            return sendVersion;
        }
    }
}
