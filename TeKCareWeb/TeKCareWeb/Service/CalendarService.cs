﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeKCareWeb.Util;
using TeKCareWeb.Vo;

namespace TeKCareWeb.Service
{
    public class CalendarService
    {
        private List<Models.Calendar> QueryCalendarList;

        public Calendar getHoliday(string toDate)
        {
            Calendar calendar = new Calendar();
            using (var ctx = new Models.CRM_StagingEntities())
            {
                var q = ctx.Calendars.Where(x1 => x1.workDate == toDate);
                if (q != null)
                {
                    QueryCalendarList = q.ToList();
                    int count = QueryCalendarList.Count();

                    if (count > 0)
                    {
                        if (QueryCalendarList[0].isHoliday.Equals("是"))
                        {
                            calendar.status = "1";
                        }
                        else
                        {
                            calendar.status = "0";
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            return calendar;
        }
    }
}
