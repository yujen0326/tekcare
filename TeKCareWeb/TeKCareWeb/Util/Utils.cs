﻿using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.DirectoryServices.Protocols;
using System.Net;

namespace TeKCareWeb.Util
{
    public class Utils
    {

        public static string EnCodeJwt(Dictionary<string, string> userInfo)
        {
            string JwtKey = System.Configuration.ConfigurationManager.AppSettings["JwtKey"];

            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();

            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);
            var token = encoder.Encode(userInfo, JwtKey);
            return token;
        }

        public static string DeCodeJwt(string token)
        {
            string JwtKey = System.Configuration.ConfigurationManager.AppSettings["JwtKey"];

            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();

            IDateTimeProvider provider = new UtcDateTimeProvider();
            IJwtValidator validator = new JwtValidator(serializer, provider);
            IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);
            var json = decoder.Decode(token, JwtKey, verify: true);
            return json;
        }


        public static string checkAccessToken(string memberId, string accessToken)
        {
            if (!accessToken.Equals(""))
            {
                string tokenTimeMinute = System.Configuration.ConfigurationManager.AppSettings["tokenTimeMinute"];
                string getUserInfo = DeCodeJwt(accessToken);

                Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(getUserInfo);
                string jsonMemberId = values["m_user"].ToString();
                string jsonTime = values["time"].ToString();

                DateTime tokenTime = Convert.ToDateTime(jsonTime);

                int intTokenTimeSeconds = Convert.ToInt32(tokenTimeMinute) * 60;
                int intDateDiff = DateDiff(tokenTime, DateTime.Now);

                if (!values["m_user"].ToString().Equals(memberId))
                {
                    return "900";
                }
                else if (intDateDiff > intTokenTimeSeconds)
                {
                    return "500";
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "900";
            }
        }

        public static int DateDiff(DateTime DateTime1, DateTime DateTime2)
        {
            int intDateDiff = 0;
            TimeSpan ts1 = new TimeSpan(DateTime1.Ticks);
            TimeSpan ts2 = new TimeSpan(DateTime2.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            intDateDiff = Convert.ToInt32(ts.TotalSeconds);

            return intDateDiff;
        }

        public static bool ValidateLDAPUser(string userId, string password)
        {
            string LdapServer = System.Configuration.ConfigurationManager.AppSettings["LdapServer"];
            string LdapPort = System.Configuration.ConfigurationManager.AppSettings["LdapPort"];
            string LdapDomain = System.Configuration.ConfigurationManager.AppSettings["LdapDomain"];
            int port = Convert.ToInt32(LdapPort);
            try
            {
                using (var ldapConnection = new LdapConnection(
                        new LdapDirectoryIdentifier(LdapServer, port)))
                {
                    ldapConnection.SessionOptions.ProtocolVersion = 3;
                    //如果啟用 Secure Sockets Layer，則這個屬性為 true，否則為 false
                    ldapConnection.SessionOptions.SecureSocketLayer = false;
                    //LDAP登入身份, 若設定為Anonymous則為匿名登入
                    ldapConnection.AuthType = AuthType.Negotiate;

                    //設定登入帳號&密碼
                    ldapConnection.Credential = new NetworkCredential(userId, password, LdapDomain);
                    //連線
                    ldapConnection.Bind();
                    return true;
                }

            }
            catch (LdapException e)
            {
                Util.Utils.errorLog("Error with ldap server " + LdapServer + " " + e.ToString(), "ldap");
                return false;
            }
        }


        public static void errorLog(string strLineArray,string fileName)
        {

            string logFileDirectoryName = System.Configuration.ConfigurationManager.AppSettings["logFileDirectoryName"];

            string fileDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\MtekcareLog\\" + logFileDirectoryName+ "\\log\\";

            //今日日期
            DateTime Date = DateTime.Now;
            string Tody = Date.ToString("yyyy-MM-dd");

            //如果此路徑沒有資料夾
            if (!Directory.Exists(fileDirectory))
            {
                //新增資料夾
                Directory.CreateDirectory(fileDirectory);
            }

            //把內容寫到目的檔案，若檔案存在則附加在原本內容之後(換行)
            File.AppendAllText(fileDirectory + Tody +"_"+ fileName  + ".txt", strLineArray + "\r\n");
        }
    }
}