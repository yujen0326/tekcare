﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TeKCareWeb.Controllers.Vo;
using TeKCareWeb.Vo;
using TeKCareWeb.Service;
using TeKCareWeb.Util;
using System.Web.Services.Description;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TeKCareWeb.Controllers.api
{
    public class UserController : ApiController
    {
        UserService userService = new UserService();

        //查詢作業
        [Route("api/getQueryUser")]
        [HttpPost]
        public UserResult QueryUser(UserRequest request)
        {
            UserResult result = new UserResult();
            try
            {
                User user = userService.getQueryUser(request.m_user);
                if (user == null)
                {
                    result.processStatus.token = "";
                    result.processStatus.resultCode = "200";
                    result.processStatus.resultMessage = "工號不存在";
                }
                else
                {
                    result.processStatus.token = "";
                    result.processStatus.resultCode = "100";
                    result.processStatus.resultMessage = "工號已存在";
                    result.result = user;
                }
            }
            catch (Exception ex)
            {
                result.processStatus.resultCode = "999";
                result.processStatus.resultMessage = ex.Message;
            }
            return result;
        }

        //新增AD帳號作業
        [Route("api/insUser")]
        [HttpPost]
        public AddUserResult insUser(AddUserRequest request)
        {
            AddUserResult result = new AddUserResult();
            try
            {
                AddUser addUser = userService.insUser(request.empId, request.m_user);

                if (addUser.addStatus.Equals("500"))
                {
                    result.processStatus.token = "";
                    result.processStatus.resultCode = "500";
                    result.processStatus.resultMessage = "此帳號AD或ID已存在";
                }
                else if(addUser.addStatus.Equals("200"))
                {
                    result.processStatus.token = "";
                    result.processStatus.resultCode = "200";
                    result.processStatus.resultMessage = "新增失敗";
                }
                else
                {
                    result.processStatus.token = "";
                    result.processStatus.resultCode = "100";
                    result.processStatus.resultMessage = "新增成功";
                    result.result = addUser;
                }
            }
            catch (Exception ex)
            {
                result.processStatus.resultCode = "999";
                result.processStatus.resultMessage = ex.Message;
            }
            return result;
        }


        //修改AD帳號作業
        [Route("api/updUser")]
        [HttpPost]
        public UpdUserResult updUser(UpdUserRequest request)
        {
            UpdUserResult result = new UpdUserResult();
            try
            {
                UpdUser updUser = userService.updUser(request.sn, request.empId, request.m_user, request.status);

                if (updUser.updStatus.Equals("500"))
                {
                    result.processStatus.token = "";
                    result.processStatus.resultCode = "500";
                    result.processStatus.resultMessage = "此帳號AD或ID已存在";
                }
                else if (updUser.updStatus.Equals("200"))
                {
                    result.processStatus.token = "";
                    result.processStatus.resultCode = "200";
                    result.processStatus.resultMessage = "修改失敗";
                }
                else
                {
                    result.processStatus.token = "";
                    result.processStatus.resultCode = "100";
                    result.processStatus.resultMessage = "修改成功";
                    result.result = updUser;
                }
            }
            catch (Exception ex)
            {
                result.processStatus.resultCode = "999";
                result.processStatus.resultMessage = ex.Message;
            }
            return result;
        }

    }
}