﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TeKCareWeb.Controllers.Vo;
using TeKCareWeb.Vo;
using TeKCareWeb.Service;
using TeKCareWeb.Util;
using System.Web.Services.Description;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TeKCareWeb.Controllers.api
{
    public class CalendarController : ApiController
    {
        CalendarService calendarService = new CalendarService();

        [Route("api/getHoliday")]
        [HttpPost]
        public CalendarResult getHoliday(CalendarRequest request)
        {
            CalendarResult result = new CalendarResult();

            try
            {
                Calendar calendar = calendarService.getHoliday(request.todate);
                if (calendar == null)
                {
                    result.processStatus.resultCode = "200";
                    result.processStatus.resultMessage = "資料無法取得";
                }
                else
                {
                    result.processStatus.resultCode = "100";
                    result.processStatus.resultMessage = "資料取得成功";
                    result.result = calendar;
                    result.processStatus.token = null;
                }
            }
            catch (Exception ex)
            {
                result.processStatus.resultCode = "999";
                result.processStatus.resultMessage = ex.Message;
            }
            return result;
        }



    }
}