﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TeKCareWeb.Controllers.Vo;
using TeKCareWeb.Vo;
using TeKCareWeb.Service;
using TeKCareWeb.Util;
using System.Web.Services.Description;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TeKCareWeb.Controllers.api
{
    public class SignController : ApiController
    {
        SignService signService = new SignService();

        //登入作業
        [Route("api/sign")]
        [HttpPost]
        public SignResult Login(SignRequest request)
        {
            SignResult result = new SignResult();

            try
            {
                Sign sign = signService.Login(request.empId, request.empPwd, request.empToken);

                if (sign == null)
                {
                    result.processStatus.resultCode = "200";
                    result.processStatus.resultMessage = "登入失敗";
                }
                else
                {
                    sign.accessToken = signService.generateToken(sign.m_user);

                    result.processStatus.token = sign.accessToken;
                    result.processStatus.resultCode = "100";
                    result.processStatus.resultMessage = "登入成功";
                    result.result = sign;

                    Util.Utils.errorLog(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), "accessToken");
                    Util.Utils.errorLog(sign.accessToken, "accessToken");
                }
            }
            catch (Exception ex)
            {
                result.processStatus.resultCode = "999";
                result.processStatus.resultMessage = ex.Message;
            }
            return result;
        }
    }
}