﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TeKCareWeb.Controllers.Vo;
using TeKCareWeb.Vo;
using TeKCareWeb.Service;
using TeKCareWeb.Util;
using System.Web.Services.Description;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TeKCareWeb.Controllers.api
{
    public class VersionController : ApiController
    {
        VersionService versionService = new VersionService();

        [Route("api/getVersion")]
        [HttpPost]
        public GetVersionResult getVersion(GetVersionRequest request)
        {
            GetVersionResult result = new GetVersionResult();

            try
            {
                GetVersion getVersion = versionService.getVersionStr();
                if (getVersion == null)
                {
                    result.processStatus.resultCode = "200";
                    result.processStatus.resultMessage = "版本資料無法取得";
                }
                else
                {
                    result.processStatus.resultCode = "100";
                    result.processStatus.resultMessage = "版本資料取得成功";
                    result.result = getVersion;
                    result.processStatus.token = null;
                }
            }
            catch (Exception ex)
            {
                result.processStatus.resultCode = "999";
                result.processStatus.resultMessage = ex.Message;
            }
            return result;
        }

        [Route("api/sendVersion")]
        [HttpPost]
        public SendVersionResult sendVersion(SendVersionRequest request)
        {
            SendVersionResult result = new SendVersionResult();

            try
            {
                SendVersion sendVersion = versionService.SendVersionStr(request.version);
                if (sendVersion == null)
                {
                    result.processStatus.resultCode = "200";
                    result.processStatus.resultMessage = "版本資料無法更新";
                }
                else
                {
                    result.processStatus.resultCode = "100";
                    result.processStatus.resultMessage = "版本資料更新成功";
                    result.result = sendVersion;
                    result.processStatus.token = null;
                }
            }
            catch (Exception ex)
            {
                result.processStatus.resultCode = "999";
                result.processStatus.resultMessage = ex.Message;
            }
            return result;
        }
    }
}