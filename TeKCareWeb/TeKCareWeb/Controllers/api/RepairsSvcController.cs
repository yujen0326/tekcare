﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TeKCareWeb.Controllers.Vo;
using TeKCareWeb.Vo;
using TeKCareWeb.Service;
using TeKCareWeb.Util;
using System.Web.Services.Description;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TeKCareWeb.Controllers.api
{
    public class RepairsSvcController : ApiController
    {
        RepairsSvcService repairsSvcService = new RepairsSvcService();
        [Route("api/getNewRepair")]
        [HttpPost]
        public RepairsSvcResult getRepairsSvc(RepairsSvcRequest request)
        {
            RepairsSvcResult result = new RepairsSvcResult();
            //repairsSvcService.setMobiletimeStaging();

            try
            {
                string accessToken = request.accessToken;

                Util.Utils.errorLog(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), "accessToken");
                Util.Utils.errorLog("使用api/getNewRepair,傳accessToken=" + accessToken, "accessToken");

                string checkAccess = Utils.checkAccessToken(request.m_user, accessToken);
                Util.Utils.errorLog(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), "accessToken");
                Util.Utils.errorLog("使用checkAccessToken,回傳checkAccess=" + checkAccess, "accessToken");

                if (checkAccess.Equals("900"))
                {
                    result.processStatus.resultCode = "900";
                    result.processStatus.resultMessage = "比對accessToken不一致。";
                }
                else if (checkAccess.Equals("500"))
                {
                    result.processStatus.resultCode = "500";
                    result.processStatus.resultMessage = "連線逾時，請重新登入。";
                }
                else
                {

                    //記log
                    Util.Utils.errorLog(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), "getRepairsSvc");
                    Util.Utils.errorLog(request.m_user, "getRepairsSvc");
                    Util.Utils.errorLog("status=1", "getRepairsSvc");

                    List<RepairsSvc> repairsSvcList = repairsSvcService.newRepairsSvc(request.m_user, "1");

                    result.processStatus.resultCode = "100";
                    result.processStatus.resultMessage = "資料取得成功";

                    result.processStatus.token = null;
                    result.result = repairsSvcList;
                }
            }
            catch (Exception ex)
            {
                result.processStatus.resultCode = "999"; // 錯誤代碼，先隨便暫定
                result.processStatus.resultMessage = ex.Message;
            }
            return result;
        }


        [Route("api/getNotCloseRepair")]
        [HttpPost]
        public RepairsSvcResult getNotCloseRepair(RepairsSvcRequest request)
        {
            RepairsSvcResult result = new RepairsSvcResult();
            try
            {
                //string accessToken = request.accessToken;

                //string checkAccess = Utils.checkAccessToken(request.m_user, accessToken);
                //Util.Utils.errorLog(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), "accessToken");
                //Util.Utils.errorLog(accessToken, "accessToken");

                //if (checkAccess.Equals("900"))
                //{
                //    result.processStatus.resultCode = "900";
                //    result.processStatus.resultMessage = "比對accessToken不一致。";
                //}
                //else if (checkAccess.Equals("500"))
                //{
                //    result.processStatus.resultCode = "500";
                //    result.processStatus.resultMessage = "連線逾時，請重新登入。";
                //}
                //else
                //{
                    //記log
                    Util.Utils.errorLog(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), "getNotCloseRepair");
                    Util.Utils.errorLog(request.m_user, "getNotCloseRepair");
                    Util.Utils.errorLog("status=2", "getNotCloseRepair");

                    List<RepairsSvc> repairsSvcList = repairsSvcService.newRepairsSvc(request.m_user, "2");

                    result.processStatus.resultCode = "100";
                    result.processStatus.resultMessage = "資料取得成功";

                    result.processStatus.token = null;
                    result.result = repairsSvcList;
                //}
            }
            catch (Exception ex)
            {
                result.processStatus.resultCode = "999"; // 錯誤代碼，先隨便暫定
                result.processStatus.resultMessage = ex.Message;
            }
            return result;
        }

        [Route("api/updateRepairStatus")]
        [HttpPost]
        public RepairsStatusResult updateRepairStatus(RepairsStatusRequest request)
        {
            RepairsStatusResult result = new RepairsStatusResult();
            try
            {
                //string accessToken = request.accessToken;

                //string checkAccess = Utils.checkAccessToken(request.m_user, accessToken);
                //Util.Utils.errorLog(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), "accessToken");
                //Util.Utils.errorLog(accessToken, "accessToken");

                //if (checkAccess.Equals("900"))
                //{
                //    result.processStatus.resultCode = "900";
                //    result.processStatus.resultMessage = "比對accessToken不一致。";
                //}
                //else if (checkAccess.Equals("500"))
                //{
                //    result.processStatus.resultCode = "500";
                //    result.processStatus.resultMessage = "連線逾時，請重新登入。";
                //}
                //else
                //{
                //記log
                Util.Utils.errorLog(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), "updateRepairStatus");
                Util.Utils.errorLog(request.m_user, "updateRepairStatus");
                Util.Utils.errorLog("repair_no=" + request.repair_no, "updateRepairStatus");
                Util.Utils.errorLog("status=" + request.status, "updateRepairStatus");
                Util.Utils.errorLog("updateDatetime=" + request.updateDatetime, "updateRepairStatus");
                Util.Utils.errorLog("progress_update_date=" + request.progress_update_date, "updateRepairStatus");

                RepairsStatus repairsStatus = repairsSvcService.updateRepairStatus(request.m_user,
                                                                                       request.repair_no,
                                                                                       request.accessToken,
                                                                                       request.status,
                                                                                       request.updateDatetime,
                                                                                       request.progress_update_date);
                    if (repairsStatus == null)
                    {
                        result.processStatus.resultCode = "200";
                        result.processStatus.resultMessage = "更新失敗";
                    }
                    else
                    {
                        result.processStatus.resultCode = "100";
                        result.processStatus.resultMessage = "更新成功";
                    }
                    result.processStatus.token = null;
                    result.result = repairsStatus;
                //}
            }
            catch (Exception ex)
            {
                result.processStatus.resultCode = "999"; // 錯誤代碼，先隨便暫定
                result.processStatus.resultMessage = ex.Message;
            }
            return result;
        }

        [Route("api/sendRepairsSvcByApp")]
        [HttpPost]
        public RepairsSvcAppResult sendRepairsSvcByApp(RepairsSvcAppRequest request)
        {
            RepairsSvcAppResult result = new RepairsSvcAppResult();
            try
            {
                //string accessToken = request.accessToken;

                //string checkAccess = Utils.checkAccessToken(request.m_user, accessToken);
                //Util.Utils.errorLog(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), "accessToken");
                //Util.Utils.errorLog(accessToken, "accessToken");

                //if (checkAccess.Equals("900"))
                //{
                //    result.processStatus.resultCode = "900";
                //    result.processStatus.resultMessage = "比對accessToken不一致。";
                //}
                //else if (checkAccess.Equals("500"))
                //{
                //    result.processStatus.resultCode = "500";
                //    result.processStatus.resultMessage = "連線逾時，請重新登入。";
                //}
                //else
                //{
                    RepairsSvcApp repairsSvcApp = repairsSvcService.sendRepairsSvcByApp(request.m_user,
                                                                                              request.accessToken,
                                                                                              request.mRepairStagingStatus);

                    result.processStatus.resultCode = "100";
                    result.processStatus.resultMessage = "資料載入APP成功";
                    result.processStatus.token = null;
                    result.result = repairsSvcApp;
               // }
            }
            catch (Exception ex)
            {
                result.processStatus.resultCode = "999"; // 錯誤代碼，先隨便暫定
                result.processStatus.resultMessage = ex.Message;
            }
            return result;
        }
    }
}