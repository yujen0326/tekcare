﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TeKCareWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Response.Redirect("~/Default.aspx");
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            Response.Redirect("~/Default.aspx");
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            Response.Redirect("~/Default.aspx");
            return View();
        }
    }
}
