﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.DirectoryServices.Protocols;
using System.Net;
using TeKCareWeb.Service;

namespace TeKCareWeb
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //RepairsSvcService a = new RepairsSvcService();
                //a.setMobiletimeStaging();

                string logFileDirectoryName = System.Configuration.ConfigurationManager.AppSettings["logFileDirectoryName"];
                string version = System.Configuration.ConfigurationManager.AppSettings["version"];

                lbl_1.Text = logFileDirectoryName + "區";
                lbl_2.Text = version;
            }
        }

        protected void btn_LDAP_Click(object sender, EventArgs e)
        {
            Message.Text = "";

            string userId = TextBox1.Text;
            string passWord = TextBox2.Text;

            if (!userId.Trim().Equals(""))
            {

                if (!passWord.Trim().Equals(""))
                {
                    bool msg = ValidateLDAPUser("172.21.2.10", 389, userId, passWord);

                    if (msg)
                    {
                        Message.Text = userId + " Successfully authenticated to ldap server ";
                    }
                    else
                    {
                        Message.Text = userId + " Error with ldap server ";
                    }
                   TextBox1.Text = "";
                   TextBox2.Text = "";

                }
            }
        }

        static bool ValidateLDAPUser(string ldapserver, int port, string userId, string password)
        {
            try
            {
                using (var ldapConnection = new LdapConnection(
                        new LdapDirectoryIdentifier(ldapserver,port)))
                {
                    ldapConnection.SessionOptions.ProtocolVersion = 3;
                    //如果啟用 Secure Sockets Layer，則這個屬性為 true，否則為 false
                    ldapConnection.SessionOptions.SecureSocketLayer = false;
                    //LDAP登入身份, 若設定為Anonymous則為匿名登入
                    ldapConnection.AuthType = AuthType.Negotiate;

                    //設定登入帳號&密碼
                    ldapConnection.Credential = new NetworkCredential(userId, password, "apo.epson.net");
                    //連線
                    ldapConnection.Bind();

                    Util.Utils.errorLog("Successfully authenticated to ldap server " + ldapserver, "ldap");
                    return true;
                }

            }
            catch (LdapException e)
            {
                Util.Utils.errorLog("Error with ldap server " + ldapserver +" "+e.ToString(), "ldap");
                return false;
            }
        }
    }
}