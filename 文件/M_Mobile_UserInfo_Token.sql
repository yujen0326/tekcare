USE [CRM_Staging]
GO
/****** Object:  Table [dbo].[M_Mobile_UserInfo_Token]    Script Date: 2019/6/6 下午 10:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[M_Mobile_UserInfo_Token](
	[sn] [int] IDENTITY(1,1) NOT NULL,
	[tek_user_id] [nvarchar](50) NOT NULL,
	[tek_user_token] [nvarchar](max) NOT NULL,
	[newDatetime] [nvarchar](50) NOT NULL,
	[status] [nvarchar](1) NOT NULL,
 CONSTRAINT [PK_M_Mobile_UserInfo_Token] PRIMARY KEY CLUSTERED 
(
	[sn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
