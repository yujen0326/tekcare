USE [CRM_Staging]
GO
/****** Object:  Table [dbo].[Repair_Staging_Push]    Script Date: 2019/6/11 上午 01:07:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Repair_Staging_Push](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[tek_repairstatus] [nvarchar](150) NOT NULL,
	[tek_name] [nvarchar](150) NOT NULL,
	[tek_recipient_date] [datetime] NOT NULL,
	[tek_m_userid] [nvarchar](150) NOT NULL,
	[tek_m_user] [nvarchar](150) NOT NULL,
	[tek_changecode] [nvarchar](150) NOT NULL,
	[push_time] [datetime] NOT NULL,
	[multicast_id] [nvarchar](50) NOT NULL,
	[success] [nvarchar](50) NOT NULL,
	[failure] [nvarchar](50) NOT NULL,
	[canonical_ids] [nvarchar](50) NOT NULL,
	[results] [nvarchar](150) NOT NULL,
	[status] [nvarchar](1) NOT NULL,
 CONSTRAINT [PK_Repair_Staging_Push] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
