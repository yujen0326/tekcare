USE [CRM_Staging]
GO
/****** Object:  Table [dbo].[M_Mobiletime_Staging]    Script Date: 2019/5/28 上午 10:31:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[M_Mobiletime_Staging](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[tek_Id] [int] NOT NULL,
	[tek_repair_tek_mobiletime] [nvarchar](150) NULL,
	[tek_m_status] [nvarchar](150) NULL,
	[tek_flag] [datetime] NULL,
	[Status] [nvarchar](20) NULL,
	[Log] [nvarchar](400) NULL,
	[tek_m_user] [nvarchar](150) NULL,
	[tek_GPS] [nvarchar](150) NULL,
	[newDatetime] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[M_Mobiletime_Staging] ADD  DEFAULT ('Waiting') FOR [Status]
GO
