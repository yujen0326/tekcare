﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FireBasePush
{
    class Program
    {
        static void Main(string[] args)
        {
            string tokenString = System.Configuration.ConfigurationManager.AppSettings["tokenString"];  //token String
            string titleString = System.Configuration.ConfigurationManager.AppSettings["titleString"];  //title String
            string bodyString = System.Configuration.ConfigurationManager.AppSettings["bodyString"];    //body String
            sendMessage(tokenString, titleString, bodyString);
        }

        //做sendMessage
        #region//sendMessage(string tokenString, string titleString, string bodyString)
        public static string sendMessage(string tokenString, string titleString, string bodyString)
        {
            string projectId = System.Configuration.ConfigurationManager.AppSettings["projectId"];  //專案 ID
            string serverKey = System.Configuration.ConfigurationManager.AppSettings["serverKey"];  //伺服器金鑰
            string sendFireBaseUrl = System.Configuration.ConfigurationManager.AppSettings["sendFireBaseUrl"];  //send FireBase Url
            string sResponseFromServer = "";

            WebRequest tRequest = WebRequest.Create(sendFireBaseUrl);
            tRequest.Method = "post";
            //serverKey - Key from Firebase cloud messaging server  
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            //Sender Id - From firebase project setting  
            tRequest.Headers.Add(string.Format("Sender: id={0}", projectId));
            tRequest.ContentType = "application/json";
            var payload = new
            {
                to = tokenString,
                priority = "high",
                content_available = true,
                notification = new
                {
                    body = bodyString,
                    title = titleString,
                    badge = 1
                },
            };

            string postbody = JsonConvert.SerializeObject(payload).ToString();
            Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
            tRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (WebResponse tResponse = tRequest.GetResponse())
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                sResponseFromServer = tReader.ReadToEnd();
                                errorLog(sResponseFromServer, "pushlog");
                            }
                    }
                }
            }
            return sResponseFromServer;
        }
        #endregion

        public static void errorLog(string strLineArray, string fileName)
        {
            string fileDirectory = AppDomain.CurrentDomain.BaseDirectory + "log\\";
            //今日日期
            DateTime Date = DateTime.Now;
            string Tody = Date.ToString("yyyy-MM-dd");

            //如果此路徑沒有資料夾
            if (!Directory.Exists(fileDirectory))
            {
                //新增資料夾
                Directory.CreateDirectory(fileDirectory);
            }

            //把內容寫到目的檔案，若檔案存在則附加在原本內容之後(換行)
            File.AppendAllText(fileDirectory + Tody + "_" + fileName + ".txt", strLineArray + "\r\n");
        }
    }
}
