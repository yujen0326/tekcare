//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace TeKCarePush
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_Repair_Staging
    {
        public int Id { get; set; }
        public Nullable<int> tek_Id { get; set; }
        public string tek_repairstatus { get; set; }
        public string tek_account { get; set; }
        public string tek_name { get; set; }
        public Nullable<System.DateTime> tek_recipient_date { get; set; }
        public string tek_service { get; set; }
        public string tek_service_item { get; set; }
        public string tek_serial_no { get; set; }
        public string tek_warrenty { get; set; }
        public string tek_contact { get; set; }
        public string tek_telephone { get; set; }
        public string tek_mobile { get; set; }
        public string tek_clientadd { get; set; }
        public string tek_note { get; set; }
        public string Status { get; set; }
        public string Log { get; set; }
        public string tek_m_user { get; set; }
        public string tek_remark { get; set; }
        public string tek_product { get; set; }
        public string tek_errordescip { get; set; }
        public string tek_m_userid { get; set; }
        public string tek_bigtype { get; set; }
        public string tek_smalltype { get; set; }
        public Nullable<System.DateTime> tek_warrantystart { get; set; }
        public Nullable<System.DateTime> tek_warrantyend { get; set; }
        public string tek_distributor { get; set; }
        public string tek_distributor_contact { get; set; }
        public string tek_distributor_phone { get; set; }
        public string tek_changecode { get; set; }
        public string isNew { get; set; }
        public string newDatetime { get; set; }
        public string isLoad { get; set; }
        public string loadDatetime { get; set; }
        public string isCancel { get; set; }
        public string cancelDatetime { get; set; }
        public string isStage { get; set; }
        public string caseType { get; set; }
        public string caseStatus { get; set; }
    }
}
