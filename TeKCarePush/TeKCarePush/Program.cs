﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TeKCarePush
{
    class Program
    {
        static void Main(string[] args)
        {
            getTokenDoSendPush();   //關於案件
            getTokenDoSendPush2();  //關於留言
        }

        public static void getTokenDoSendPush()
        {
            string api_tek_name = "";
            string api_tek_repairstatus = "";
            string titleString = "";
            string bodyString = "";
            string push_tek_user_id = "";
            string push_tek_user_token = "";
            string tek_m_userid = "";
            string tek_m_user = "";
            string pushTimeStr = "";


            int b = 0; //維修中
            int c = 0; //取消維修
            int d = 0; //重派

            List<M_Mobile_UserInfo_Token> mMobileUserInfoTokenList = new List<M_Mobile_UserInfo_Token>();
            List<M_Repair_Staging> mRepairStagingList = new List<M_Repair_Staging>();
            using (var ctx = new CRM_StagingEntities())
            {
                var q = ctx.M_Mobile_UserInfo_Token.Where(x1 => x1.status == "1");
                if (q != null)
                {
                    mMobileUserInfoTokenList = q.ToList();
                    int count = mMobileUserInfoTokenList.Count();
                    if (count > 0)
                    {
                        for (int i = 0; i < count; i++)
                        {
                            api_tek_name = "";
                            b = 0; //維修中
                            c = 0; //取消維修
                            d = 0; //重派

                            push_tek_user_id = mMobileUserInfoTokenList[i].tek_user_id;
                            push_tek_user_token = mMobileUserInfoTokenList[i].tek_user_token;

                            List<Repair_Staging_Push> repairStagingPushList = new List<Repair_Staging_Push>();

                            //檢查Insert
                            TeKCarePush.tekcare.Tek_WebMobileSvc api = new TeKCarePush.tekcare.Tek_WebMobileSvc();
                            Object o = api.GetRepairsSvc(push_tek_user_id, "1");

                            JArray objFirst = (JArray)JsonConvert.DeserializeObject(o.ToString());

                            JArray obj = new JArray(objFirst.OrderBy(x => (string)x["維修單號"]));

                            List<RepairStaging> repairStagingList = new List<RepairStaging>();

                            foreach (JObject content in obj.Children<JObject>())
                            {
                                RepairStaging api1 = new RepairStaging();

                                foreach (JProperty prop in content.Properties())
                                {
                                    string tempName = prop.Name.ToString();
                                    string tempValue = prop.Value.ToString();

                                    if (tempName.Equals("ID")) { api1.tek_id = tempValue; }
                                    if (tempName.Equals("維修狀態")) { api1.tek_repairstatus = tempValue; }
                                    if (tempName.Equals("客戶名稱")) { api1.tek_account = tempValue; }
                                    if (tempName.Equals("維修單號")) { api1.tek_name = tempValue; }
                                    if (tempName.Equals("維修開始時間")) { api1.tek_recipient_date = tempValue; }
                                    if (tempName.Equals("工程師")) { api1.tek_m_user = tempValue; }
                                    if (tempName.Equals("工程師ID")) { api1.tek_m_userid = tempValue; }
                                    if (tempName.Equals("更新狀態")) { api1.tek_changecode = tempValue; }
                                }
                                if (api1.tek_changecode.Equals("Insert"))
                                {
                                    if ((api1.tek_name.Equals(api_tek_name)) && (api1.tek_repairstatus.Equals(api_tek_repairstatus)))
                                    {
                                    }
                                    else
                                    { 
                                        api_tek_name = api1.tek_name;
                                        api_tek_repairstatus = api1.tek_repairstatus;
                                        //先查暫存資料表
                                        DataSet ds = getMRepairStaging(api1.tek_name, push_tek_user_id);
                                        int dsCount = ds.Tables[0].Rows.Count;
                                        if (dsCount > 0)
                                        {

                                            //檢查推播資料表
                                            DataSet ds1 = getRepairStagingPush(api1.tek_name, push_tek_user_id, api1.tek_repairstatus, api1.tek_changecode);
                                            int dsCount1 = ds1.Tables[0].Rows.Count;
                                            if (dsCount1 > 0)
                                            {
                                                //有推播不需要再推播
                                            }
                                            else
                                            {
                                                tek_m_userid = api1.tek_m_userid;
                                                tek_m_user = api1.tek_m_user;
                                                Repair_Staging_Push repairStagingPush = new Repair_Staging_Push();
                                                //推播
                                                if (api1.tek_repairstatus.Equals("維修中")) { b = b + 1; }
                                                if (api1.tek_repairstatus.Equals("取消維修")) { c = c + 1; }
                                                repairStagingPush.tek_id = api1.tek_id;
                                                repairStagingPush.tek_repairstatus = api1.tek_repairstatus;
                                                repairStagingPush.tek_account = api1.tek_account;
                                                repairStagingPush.tek_name = api1.tek_name;
                                                repairStagingPush.tek_recipient_date = Convert.ToDateTime(api1.tek_recipient_date);
                                                repairStagingPush.tek_m_user = api1.tek_m_user;
                                                repairStagingPush.tek_m_userid = api1.tek_m_userid;
                                                repairStagingPush.tek_changecode = api1.tek_changecode;
                                                repairStagingPushList.Add(repairStagingPush);
                                            }

                                        }
                                        else
                                        {
                                            //檢查推播資料表
                                            DataSet ds1 = getRepairStagingPush(api1.tek_name, push_tek_user_id, api1.tek_repairstatus, api1.tek_changecode);
                                            int dsCount1 = ds1.Tables[0].Rows.Count;
                                            if (dsCount1 > 0)
                                            {
                                                //有推播不需要再推播
                                            }
                                            else
                                            {
                                                tek_m_userid = api1.tek_m_userid;
                                                tek_m_user = api1.tek_m_user;
                                                Repair_Staging_Push repairStagingPush = new Repair_Staging_Push();
                                                //推播
                                                if (api1.tek_repairstatus.Equals("維修中")) { b = b + 1; }
                                                if (api1.tek_repairstatus.Equals("取消維修")) { c = c + 1; }
                                                repairStagingPush.tek_id = api1.tek_id;
                                                repairStagingPush.tek_repairstatus = api1.tek_repairstatus;
                                                repairStagingPush.tek_account = api1.tek_account;
                                                repairStagingPush.tek_name = api1.tek_name;
                                                repairStagingPush.tek_recipient_date = Convert.ToDateTime(api1.tek_recipient_date);
                                                repairStagingPush.tek_m_user = api1.tek_m_user;
                                                repairStagingPush.tek_m_userid = api1.tek_m_userid;
                                                repairStagingPush.tek_changecode = api1.tek_changecode;
                                                repairStagingPushList.Add(repairStagingPush);
                                            }
                                        }
                                    }
                                }
                            }

                            api_tek_name = "";
                            //檢查Update
                            Object o2 = api.GetRepairsSvc(push_tek_user_id, "1");
                            JArray objFirst2 = (JArray)JsonConvert.DeserializeObject(o2.ToString());

                            JArray obj2 = new JArray(objFirst2.OrderBy(x => (string)x["維修單號"]));

                            List<RepairStaging> repairStagingList2 = new List<RepairStaging>();

                            foreach (JObject content2 in obj2.Children<JObject>())
                            {
                                RepairStaging api2 = new RepairStaging();

                                foreach (JProperty prop2 in content2.Properties())
                                {
                                    string tempName2 = prop2.Name.ToString();
                                    string tempValue2 = prop2.Value.ToString();

                                    if (tempName2.Equals("ID")) { api2.tek_id = tempValue2; }
                                    if (tempName2.Equals("維修狀態")) { api2.tek_repairstatus = tempValue2; }
                                    if (tempName2.Equals("客戶名稱")) { api2.tek_account = tempValue2; }
                                    if (tempName2.Equals("維修單號")) { api2.tek_name = tempValue2; }
                                    if (tempName2.Equals("維修開始時間")) { api2.tek_recipient_date = tempValue2; }
                                    if (tempName2.Equals("工程師")) { api2.tek_m_user = tempValue2; }
                                    if (tempName2.Equals("工程師ID")) { api2.tek_m_userid = tempValue2; }
                                    if (tempName2.Equals("更新狀態")) { api2.tek_changecode = tempValue2; }
                                }


                                if (api2.tek_repairstatus.Equals("維修中"))
                                {
                                    if (api2.tek_changecode.Equals("Update"))
                                    {
                                        //if (!api2.tek_name.Equals(api_tek_name))
                                        //{
                                        DateTime t1 = Convert.ToDateTime(api2.tek_recipient_date);
                                        if (t1 > Convert.ToDateTime("2019/08/07"))
                                        {
                                            api_tek_name = api2.tek_name;
                                            repairStagingList.Add(api2);
                                        }
                                        //}
                                    }
                                }
                            }

                            int updateCount = repairStagingList.Count;
                            if (updateCount > 0)
                            {
                                for (int z2 = 0; z2 < repairStagingList.Count; z2++)
                                {
                                    //檢查推播資料表
                                    DataSet ds2 = getRepairStagingUpdatePush(repairStagingList[z2].tek_id, repairStagingList[z2].tek_name, push_tek_user_id, repairStagingList[z2].tek_repairstatus, "Update");
                                    int dsCount2 = ds2.Tables[0].Rows.Count;
                                    if (dsCount2 > 0)
                                    {
                                        //有推播不需要再推播
                                    }
                                    else
                                    {
                                        tek_m_userid = repairStagingList[z2].tek_m_userid;
                                        tek_m_user = repairStagingList[z2].tek_m_user;
                                        Repair_Staging_Push repairStagingPush = new Repair_Staging_Push();
                                        //推播
                                        if (repairStagingList[z2].tek_repairstatus.Equals("維修中")) { d = d + 1; }
                                        repairStagingPush.tek_id = repairStagingList[z2].tek_id;
                                        repairStagingPush.tek_repairstatus = repairStagingList[z2].tek_repairstatus;
                                        repairStagingPush.tek_account = repairStagingList[z2].tek_account;
                                        repairStagingPush.tek_name = repairStagingList[z2].tek_name;
                                        repairStagingPush.tek_recipient_date = Convert.ToDateTime(repairStagingList[z2].tek_recipient_date);
                                        repairStagingPush.tek_m_user = repairStagingList[z2].tek_m_user;
                                        repairStagingPush.tek_m_userid = repairStagingList[z2].tek_m_userid;
                                        repairStagingPush.tek_changecode = repairStagingList[z2].tek_changecode;
                                        repairStagingPushList.Add(repairStagingPush);
                                    }
                                }
                            }

                            if ((b > 0) || (c > 0) || (d > 0))
                            {
                                //titleString = "工號：" + tek_m_userid + ", 姓名：" + tek_m_user;
                                //bodyString = "";
                                //if (b > 0) { bodyString += "您有 " + b.ToString() + "筆新案件,"; }
                                //if (c > 0) { bodyString += "您有 " + c.ToString() + "筆取消維修,"; }
                                //if (d > 0) { bodyString += "您有 " + d.ToString() + "筆案件重派,"; }

                                //pushTimeStr = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                                //bodyString += pushTimeStr;

                                //string push = sendMessage(push_tek_user_token, titleString, bodyString); //執行推播

                                //JObject jo = (JObject)JsonConvert.DeserializeObject(push);
                                //string multicast_id = jo["multicast_id"].ToString();
                                //string success = jo["success"].ToString();
                                //string failure = jo["failure"].ToString();
                                //string canonical_id = jo["canonical_ids"].ToString();
                                //string results = jo["results"].ToString();

                                for (int z = 0; z < repairStagingPushList.Count; z++)
                                {
                                    titleString = "";
                                    bodyString = "";
                                    //推播
                                    if (repairStagingPushList[z].tek_repairstatus.Equals("維修中"))
                                    {
                                        if (repairStagingPushList[z].tek_changecode.Equals("Update"))
                                        {
                                            //重派
                                            titleString = "(重派)";
                                        }
                                        else
                                        {
                                            //新案件
                                            titleString = "(新案件)";
                                        }
                                    }
                                    else if(repairStagingPushList[z].tek_repairstatus.Equals("取消維修"))
                                    {
                                        //轉派
                                        titleString = "(轉派)";
                                    }
                                    pushTimeStr = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

                                    titleString += "維修單號：" + repairStagingPushList[z].tek_name;
                                    bodyString += "客戶名稱：" + repairStagingPushList[z].tek_account;
                                    bodyString += ",收件日期：" + repairStagingPushList[z].tek_recipient_date.ToString("yyyy/MM/dd HH:mm:ss");
                                    bodyString += ",系統時間：" + pushTimeStr;

                                    string push = sendMessage(push_tek_user_token, titleString, bodyString); //執行推播
                                    //string push = "0";
                                    JObject jo = (JObject)JsonConvert.DeserializeObject(push);
                                    string multicast_id = jo["multicast_id"].ToString();
                                    string success = jo["success"].ToString();
                                    string failure = jo["failure"].ToString();
                                    string canonical_id = jo["canonical_ids"].ToString();
                                    string results = jo["results"].ToString();

                                    Repair_Staging_Push r = new Repair_Staging_Push();
                                    r.tek_id = repairStagingPushList[z].tek_id.ToString();
                                    r.tek_repairstatus = repairStagingPushList[z].tek_repairstatus;
                                    r.tek_account = repairStagingPushList[z].tek_account;
                                    r.tek_name = repairStagingPushList[z].tek_name;
                                    r.tek_recipient_date = repairStagingPushList[z].tek_recipient_date;
                                    r.tek_m_userid = repairStagingPushList[z].tek_m_userid;
                                    r.tek_m_user = repairStagingPushList[z].tek_m_user;
                                    r.tek_changecode = repairStagingPushList[z].tek_changecode;
                                    r.push_time = Convert.ToDateTime(pushTimeStr);
                                    r.multicast_id = multicast_id;
                                    r.success = success;
                                    r.failure = failure;
                                    r.canonical_ids = canonical_id;
                                    r.results = results;
                                    r.status = "1";
                                    ctx.Repair_Staging_Push.Add(r);
                                    int z1 = ctx.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void getTokenDoSendPush2()
        {
            string titleString = "";
            string bodyString = "";
            string push_tek_user_id = "";
            string push_tek_user_token = "";
            string pushTimeStr = "";

            List<OnsiteNotesSvc> onsiteNotesSvcList = new List<OnsiteNotesSvc>();
            List<M_Mobile_UserInfo_Token> mMobileUserInfoTokenList = new List<M_Mobile_UserInfo_Token>();
            List<Onsitenote_Staging_Push> QueryOnsitenoteStagingPushList = new List<Onsitenote_Staging_Push>();

            using (var ctx = new CRM_StagingEntities())
            {
                var q = ctx.M_Mobile_UserInfo_Token.Where(x1 => x1.status == "1");
                if (q != null)
                {
                    mMobileUserInfoTokenList = q.ToList();
                    int count = mMobileUserInfoTokenList.Count();
                    if (count > 0)
                    {
                        for (int i = 0; i < count; i++)
                        {
                            push_tek_user_id = mMobileUserInfoTokenList[i].tek_user_id;
                            push_tek_user_token = mMobileUserInfoTokenList[i].tek_user_token;

                            DataSet ds = getOnsitenoteStaging(push_tek_user_id);
                            int count1 = ds.Tables[0].Rows.Count;
                            if (count1 > 0)
                            {
                                for (int j = 0; j < count1; j++)
                                {
                                    string str_tek_Id = ds.Tables[0].Rows[j]["Id"].ToString();
                                    string tek_serviceaccount = ds.Tables[0].Rows[j]["tek_serviceaccount"].ToString();
                                    string tek_workorder_no = ds.Tables[0].Rows[j]["tek_workorder_no"].ToString();
                                    string tek_repair_no = ds.Tables[0].Rows[j]["tek_repair_no"].ToString();
                                    string str_createdon = ds.Tables[0].Rows[j]["createdon"].ToString();
                                    int tek_Id = Convert.ToInt32(str_tek_Id);
                                    DateTime createdon = Convert.ToDateTime(str_createdon);

                                    var q2 = ctx.Onsitenote_Staging_Push.Where(x1 => x1.tek_Id == tek_Id);
                                    if (q2 != null)
                                    {
                                        QueryOnsitenoteStagingPushList = q2.ToList();
                                        int count2 = QueryOnsitenoteStagingPushList.Count();
                                        if (count2 > 0)
                                        {
                                            //有推播不需要再推播
                                        }
                                        else
                                        {
                                            titleString = "(新留言)";
                                            pushTimeStr = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                                            titleString += "維修單號：" + tek_repair_no;
                                            bodyString += "客戶名稱：" + tek_serviceaccount;
                                            bodyString += ",收件日期：" + createdon.ToString("yyyy/MM/dd HH:mm:ss");
                                            bodyString += ",系統時間：" + pushTimeStr;

                                            string push = sendMessage(push_tek_user_token, titleString, bodyString); //執行推播

                                            JObject jo = (JObject)JsonConvert.DeserializeObject(push);
                                            string multicast_id = jo["multicast_id"].ToString();
                                            string success = jo["success"].ToString();
                                            string failure = jo["failure"].ToString();
                                            string canonical_id = jo["canonical_ids"].ToString();
                                            string results = jo["results"].ToString();

                                            Onsitenote_Staging_Push r = new Onsitenote_Staging_Push();
                                            r.tek_Id = tek_Id;
                                            r.tek_serviceaccount = tek_serviceaccount;
                                            r.tek_workorder_no = tek_workorder_no;
                                            r.tek_repair_no = tek_repair_no;
                                            r.tek_notetype = "";
                                            r.tek_note = "";
                                            r.tek_m_user = push_tek_user_id;
                                            r.push_time = Convert.ToDateTime(pushTimeStr);
                                            r.multicast_id = multicast_id;
                                            r.success = success;
                                            r.failure = failure;
                                            r.canonical_ids = canonical_id;
                                            r.results = results;
                                            r.status = "1";
                                            ctx.Onsitenote_Staging_Push.Add(r);
                                            int z2 = ctx.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //做sendMessage
        #region//sendMessage(string tokenString, string titleString, string bodyString)
            public static string sendMessage(string tokenString, string titleString, string bodyString)
        {
            string projectId = System.Configuration.ConfigurationManager.AppSettings["projectId"];  //專案 ID
            string serverKey = System.Configuration.ConfigurationManager.AppSettings["serverKey"];  //伺服器金鑰
            string sendFireBaseUrl = System.Configuration.ConfigurationManager.AppSettings["sendFireBaseUrl"];  //send FireBase Url
            string sResponseFromServer = "";

            WebRequest tRequest = WebRequest.Create(sendFireBaseUrl);
            tRequest.Method = "post";
            //serverKey - Key from Firebase cloud messaging server  
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            //Sender Id - From firebase project setting  
            tRequest.Headers.Add(string.Format("Sender: id={0}", projectId));
            tRequest.ContentType = "application/json";
            var payload = new
            {
                to = tokenString,
                priority = "high",
                content_available = true,
                notification = new
                {
                    body = bodyString,
                    title = titleString,
                    badge = 1
                },
            };

            string postbody = JsonConvert.SerializeObject(payload).ToString();
            Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
            tRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (WebResponse tResponse = tRequest.GetResponse())
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                sResponseFromServer = tReader.ReadToEnd();
                                errorLog(sResponseFromServer, "pushlog");
                            }
                    }
                }
            }
            return sResponseFromServer;
        }
        #endregion

        //做資料查詢,回傳DataSet
        #region//getRepairStagingPush(T-SQL);傳入:strSql;回傳:DataSet
        public static DataSet getRepairStagingPush(string tek_name, string tek_m_userid, string tek_repairstatus, string tek_changecode)
        {
            DataSet ds = new DataSet();
            string strSql = "";
            try
            {
                strSql += "select * from Repair_Staging_Push ";
                strSql += "where tek_name='" + tek_name + "' ";
                strSql += "and tek_m_userid='" + tek_m_userid + "' ";
                strSql += "and tek_repairstatus='" + tek_repairstatus + "' ";
                strSql += "and tek_changecode='" + tek_changecode + "' ";
                strSql += "and status = '1'";
                ds = getDataSet(strSql);
            }
            catch (Exception ex)
            {
                errorLog(ex.StackTrace.ToString(), "errorLog");
            }
            return ds;
        }
        #endregion

        //做資料查詢,回傳DataSet
        #region//getRepairStagingUpdatePush(T-SQL);傳入:strSql;回傳:DataSet
        public static DataSet getRepairStagingUpdatePush(string tek_id, string tek_name, string tek_m_userid, string tek_repairstatus, string tek_changecode)
        {
            DataSet ds = new DataSet();
            string strSql = "";
            try
            {
                strSql += "select * from Repair_Staging_Push ";
                strSql += "where tek_name='" + tek_name + "' ";
                strSql += "and tek_m_userid='" + tek_m_userid + "' ";
                strSql += "and tek_repairstatus='" + tek_repairstatus + "' ";
                strSql += "and tek_changecode='" + tek_changecode + "' ";
                strSql += "and tek_id='" + tek_id + "' ";
                strSql += "and status = '1'";
                ds = getDataSet(strSql);
            }
            catch (Exception ex)
            {
                errorLog(ex.StackTrace.ToString(), "errorLog");
            }
            return ds;
        }
        #endregion


        //做資料查詢,回傳DataSet
        #region//getMRepairStaging(string tek_name, string tek_m_userid);傳入:strSql;回傳:DataSet
        public static DataSet getMRepairStaging(string tek_name, string tek_m_userid)
        {
            DataSet ds = new DataSet();
            string strSql = "";
            try
            {
                strSql += "select tek_repairstatus, tek_name, tek_m_userid, isLoad, isCancel, cancelDatetime from M_Repair_Staging ";
                strSql += "where tek_name='" + tek_name + "' ";
                strSql += "and tek_m_userid='" + tek_m_userid + "' ";
                ds = getDataSet(strSql);
            }
            catch (Exception ex)
            {
                errorLog(ex.StackTrace.ToString(), "errorLog");
            }
            return ds;
        }
        #endregion



        //做資料查詢,回傳DataSet
        #region//getOnsitenoteStaging(T-SQL);傳入:strSql;回傳:DataSet
        public static DataSet getOnsitenoteStaging(string tek_m_user)
        {
            DataSet ds = new DataSet();
            string strSql = "";
            try
            {
                strSql += "select ";
                strSql += "Id,";
                strSql += "tek_serviceaccount,";
                strSql += "tek_workorder_no,";
                strSql += "tek_repair_no,";
                strSql += "tek_notetype,";
                strSql += "tek_note,";
                strSql += "createdon ";
                strSql += "from ";
                strSql += "(select ";
                strSql += "Id,";
                strSql += "tek_serviceaccount,";
                strSql += "tek_workorder_no,";
                strSql += "tek_repair_no,";
                strSql += "tek_notetype,";
                strSql += "tek_note,";
                strSql += "createdon ";
                strSql += "from Onsitenote_Staging ";
                strSql += "where tek_m_user = '" + tek_m_user + "' ";
                strSql += ") a left outer join(";
                strSql += "select tek_Id from M_Onsitenote_Staging ";
                strSql += "where isLoad = '1' and tek_m_user = '" + tek_m_user + "' ";
                strSql += ") b on a.Id = b.tek_Id ";
                strSql += "where tek_Id is null ";
                ds = getDataSet(strSql);
            }
            catch (Exception ex)
            {
                errorLog(ex.StackTrace.ToString(), "errorLog");
            }
            return ds;
        }
        #endregion




        //做資料查詢,回傳DataSet
        #region//getDataSet(T-SQL);傳入:strSql;回傳:DataSet
        public static DataSet getDataSet(string sql)
        {
            string sqlConnstr = System.Configuration.ConfigurationManager.ConnectionStrings["CRM_StagingConnectionString"].ConnectionString;
            string strSql = sql;

            SqlConnection conn = new SqlConnection(sqlConnstr);
            SqlDataAdapter sda = new SqlDataAdapter(strSql, conn);

            DataSet ds = new DataSet();
            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                sda.Fill(ds, "Table");
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            finally
            {
                conn.Close();
            }
            return ds;
        }
        #endregion

        public static void errorLog(string strLineArray, string fileName)
        {
            string fileDirectory = AppDomain.CurrentDomain.BaseDirectory + "log\\";
            //今日日期
            DateTime Date = DateTime.Now;
            string Tody = Date.ToString("yyyy-MM-dd");

            //如果此路徑沒有資料夾
            if (!Directory.Exists(fileDirectory))
            {
                //新增資料夾
                Directory.CreateDirectory(fileDirectory);
            }

            //把內容寫到目的檔案，若檔案存在則附加在原本內容之後(換行)
            File.AppendAllText(fileDirectory + Tody + "_" + fileName + ".txt", strLineArray + "\r\n");
        }
        public partial class RepairStaging
        {
            public int Id { get; set; }
            public string tek_id { get; set; }
            public string tek_repairstatus { get; set; }
            public string tek_account { get; set; }
            public string tek_name { get; set; }
            public string tek_recipient_date { get; set; }
            public string tek_m_userid { get; set; }
            public string tek_m_user { get; set; }
            public string tek_changecode { get; set; }
        }

        public partial class OnsiteNotesSvc
        {
            public string tek_Id { get; set; }
            public string tek_serviceaccount { get; set; }
            public string tek_workorder_no { get; set; }
            public string tek_repair_no { get; set; }
            public string tek_notetype { get; set; }
            public string tek_note { get; set; }
            public string tek_m_user { get; set; }
            public string tek_m_user_token { get; set; }
            public string createdby { get; set; }
        }

    }
}
